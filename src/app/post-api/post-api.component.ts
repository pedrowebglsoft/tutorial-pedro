import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable, tap, of } from 'rxjs';

@Component({
  selector: 'app-post-api',
  templateUrl: './post-api.component.html',
  styleUrls: ['./post-api.component.scss']
})
export class PostAPIComponent implements OnInit {
  @Output() listData = new EventEmitter<Record<string,string>>();
  postResponse$: Observable<Record<string, string>> = of({});

  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
  }

  post() {
    this.postResponse$ = this.apiService.postToApi().pipe(
      tap(res => {
        this.listData.emit(res);
      }
    )
    );
  }
}