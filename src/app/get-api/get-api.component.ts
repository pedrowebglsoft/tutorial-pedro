import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Observable, map, of } from 'rxjs';

@Component({
  selector: 'app-get-api',
  templateUrl: './get-api.component.html',
  styleUrls: ['./get-api.component.scss']
})
export class GetAPIComponent implements OnInit {
  getResponse$: Observable<Record<string, string>[]> = of([]);
  headers: string[] = [];


  constructor(
    private apiService: ApiService
  ) { }

  ngOnInit(): void {
  }

  get() {
    this.getResponse$ = this.apiService.getFromApi().pipe(
      map(res => {
        this.headers = Object.keys(res['keyword_mapping'][0]);
        return res['keyword_mapping']})
    );
  }

}
