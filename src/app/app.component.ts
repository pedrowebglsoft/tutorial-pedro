import { Component } from '@angular/core';
import { ApiService } from './api.service';
import { Observable, map, of } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'call-api';
  

  constructor(
    private apiService: ApiService
  ) { }

}
